## Android Basics Nanodegree

I will keep here all my Android Basics Nanodegree projects that I submit, to help other students and to improve my code.

## Single Screen App
![Image](https://gitlab.com/fabiotrilhopereira/Android-Basics-Nanodegree/raw/master/singlescreen.png)

## Score Keeper App
![Image](https://gitlab.com/fabiotrilhopereira/Android-Basics-Nanodegree/raw/master/scorekeeper.png)

## User Profile App
![Image](https://gitlab.com/fabiotrilhopereira/Android-Basics-Nanodegree/raw/master/userprofile.png)

## Quiz App
![Image](https://gitlab.com/fabiotrilhopereira/Android-Basics-Nanodegree/raw/master/quizapp.png)

## Music App
![Image](https://gitlab.com/fabiotrilhopereira/Android-Basics-Nanodegree/raw/master/musicapp.gif)

## Tour Guide App
![Image](https://gitlab.com/fabiotrilhopereira/Android-Basics-Nanodegree/raw/master/tourguide.gif)
